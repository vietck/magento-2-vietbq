<?php

namespace VietBQ\Articles\Controller\Home;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Detail extends Action
{
    const REGISTRY_KEY_ARTICLE_ID = 'vietbq_articles_article_id';

    protected $_pageFactory;
    protected $_registry;

    public function __construct(Context $context,
                                PageFactory $pageFactory,
                                Registry $registry)
    {

        $this->_registry = $registry;
        $this->_pageFactory = $pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->_registry->register(self::REGISTRY_KEY_ARTICLE_ID, (int)$this->_request->getParam('id'));
        return $this->_pageFactory->create();
    }
}