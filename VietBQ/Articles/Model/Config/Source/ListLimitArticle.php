<?php

namespace VietBQ\Articles\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class ListLimitArticle implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 6, 'label' => __('6')],
            ['value' => 12, 'label' => __('12')],
            ['value' => 24, 'label' => __('24')],
        ];
    }
}