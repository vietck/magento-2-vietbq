<?php
namespace VietBQ\Articles\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Article extends AbstractModel implements IdentityInterface
{
    const CACHE_TAG = 'vietbq_articles_article';

    protected $_cacheTag = 'vietbq_articles_article';

    protected $_eventPrefix = 'vietbq_articles_article';

    protected function _construct()
    {
        $this->_init('VietBQ\Articles\Model\ResourceModel\Article');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}