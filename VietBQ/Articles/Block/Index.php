<?php

namespace VietBQ\Articles\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use VietBQ\Articles\Model\ResourceModel\Article\CollectionFactory;
use VietBQ\Articles\Model\Article;
use VietBQ\Articles\Helper\Data;

class Index extends Template
{
    protected $_articleCollectionFactory = null;
    protected $_limit;

    public function __construct(Context $context,
                                CollectionFactory $articleCollectionFactory,
                                Data $helperData)
    {
        $this->_limit = (int) $helperData->getGeneralConfig('limit');
        $this->_articleCollectionFactory = $articleCollectionFactory;
        return parent::__construct($context);
    }

    public function loadAllArticles()
    {
        $page = ($this->getRequest()->getParam('p')) ?
            $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit')) ?
            $this->getRequest()->getParam('limit') : $this->_limit;
        $articleCollection = $this->_articleCollectionFactory->create();
        $articleCollection->setOrder('article_id', 'ASC');
        $articleCollection->setPageSize($pageSize);
        $articleCollection->setCurPage($page);
        return $articleCollection;
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('Articles'));
        if ($this->loadAllArticles()) {
            $pager = $this->getLayout()
                ->createBlock('Magento\Theme\Block\Html\Pager', 'articles.home')
                ->setAvailableLimit([
                    $this->_limit => $this->_limit,
                    $this->_limit*2 => $this->_limit*2,
                    $this->_limit*3 => $this->_limit*3
                ])
                ->setShowPerPage(true)->setCollection($this->loadAllArticles());
            $this->setChild('pager', $pager);
            $this->loadAllArticles()->load();
        }
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getArticleURL(Article $article)
    {
        return '/articles/home/detail/id/' . $article->getArticleId();
    }
}