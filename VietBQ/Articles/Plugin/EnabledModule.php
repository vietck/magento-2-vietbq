<?php

namespace VietBQ\Articles\Plugin;

use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;
use VietBQ\Articles\Helper\Data;

class EnabledModule
{
    private $helperData;
    private $resultFactory;

    public function __construct(Data $helperData,
                                ResultFactory $resultFactory)
    {
        $this->helperData = $helperData;
        $this->resultFactory = $resultFactory;
    }

    public function aroundExecute(Action $subject, callable $proceed)
    {
        if ($this->helperData->getGeneralConfig('enable') == 0) {
            /**
             * @var \Magento\Framework\Controller\Result\Redirect $result
             */

            $result = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $result->setPath('/');

            return $result;
        }

        return $proceed();
    }
}