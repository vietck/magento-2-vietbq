## * LƯU Ý
Khi tùy chỉnh cấu hình module, vui lòng làm mới bộ nhớ cache theo hướng dẫn bên dưới để các thay đổi cấu hình có hiệu lực:

* Vào mục quản lý cache trong trang quản trị theo đường dẫn: **SYSTEM** -> **Cache Management**

* Đánh dấu tích vào 2 loại cache (Cache Type) có trạng thái (Status) là **INVALIDATED**:
  * **Configuration**
  * **Page Cache**

* Chọn **Refresh** trong bảng chọn và nhấn vào nút **Submit** để hoàn tất.